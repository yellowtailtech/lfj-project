############ Yellow Tail Tech #############
#        Project # 1 
#        Verify Folder Structure 
#        version 1 
####################################################

#!/bin/bash

# Function to check if a directory exists

check_directory() {
    if [ -d "$1" ]; then
        echo "pass"
    else
        echo "fail"
    fi
}

# Function to check if a file exists

check_file() {
    if [ -f "$1" ]; then
        echo "pass"
    else
        echo "fail"
    fi
}

# Getting User name from Terminal 
#usr="$USER"

# Verify Enrollment team

enrollment_home=$(check_directory "/yellow-tail-tech/enrollment/home/")
enrollment_scripts=$(check_directory "/yellow-tail-tech/enrollment/scripts/")
enrollment_docs=$(check_directory "/yellow-tail-tech/enrollment/docs/")
enrollment_readme=$(check_file "/yellow-tail-tech/enrollment/home/README")
start_enrollment=$(check_file "/yellow-tail-tech/enrollment/scripts/start_enrollment.py")
stop_enrollment=$(check_file "/yellow-tail-tech/enrollment/scripts/stop_enrollment.py")
enrollment_data=$(check_file "/yellow-tail-tech/enrollment/docs/enrollment_data.csv")
enrollment_sop=$(check_file "/yellow-tail-tech/enrollment/docs/sop.docs")
enrollment_template=$(check_file "/yellow-tail-tech/enrollment/docs/sop.template")

# Verify Student Success team

success_home=$(check_directory "/yellow-tail-tech/success/home/")
success_students=$(check_directory "/yellow-tail-tech/success/students/")
success_students_david=$(check_file "/yellow-tail-tech/success/students/david")
success_students_juan=$(check_file "/yellow-tail-tech/success/students/juan")
success_students_paul=$(check_file "/yellow-tail-tech/success/students/paul")
success_docs=$(check_directory "/yellow-tail-tech/success/docs/")
success_readme=$(check_file "/yellow-tail-tech/success/home/README")
success_sop=$(check_file "/yellow-tail-tech/success/docs/sop.docs")
success_template=$(check_file "/yellow-tail-tech/success/docs/sop.template")

# Verify Infrastructure Team

infrastructure_home=$(check_directory "/yellow-tail-tech/infrastructure/home/")
infrastructure_admin=$(check_directory "/yellow-tail-tech/infrastructure/home/admin/")
infrastructure_scripts=$(check_directory "/yellow-tail-tech/infrastructure/scripts/")
infrastructure_etc=$(check_directory "/yellow-tail-tech/infrastructure/etc/")
infrastructure_logs=$(check_directory "/yellow-tail-tech/infrastructure/logs/")
infrastructure_bin=$(check_directory "/yellow-tail-tech/infrastructure/bin/")
infrastructure_readme=$(check_file "/yellow-tail-tech/infrastructure/home/README")
vm_deploy=$(check_file "/yellow-tail-tech/infrastructure/scripts/vm_deploy.ymal")
vm_destroy=$(check_file "/yellow-tail-tech/infrastructure/scripts/vm_destroy.ymal")
etc_hosts=$(check_file "/yellow-tail-tech/infrastructure/etc/hosts.tmp")
etc_fstab=$(check_file "/yellow-tail-tech/infrastructure/etc/fstab.tmp")

# Print the results

echo "Verification Folders"
echo "-------------------------"
echo "----   Enrollment team  ----"
echo "-------------------------"
echo
echo "Directory:"
echo "home: $enrollment_home"
echo "scripts: $enrollment_scripts"
echo "docs: $enrollment_docs"
echo
echo "File:"
echo "README: $enrollment_readme"
echo "start_enrollment.py: $start_enrollment"
echo "stop_enrollment.py: $stop_enrollment"
echo "enrollment_data.csv: $enrollment_data"
echo "sop.docs: $enrollment_sop"
echo "sop.template: $enrollment_template"
echo


echo "-------------------------"
echo "---- Student Success team ----"
echo "-------------------------"
echo 
echo "Directory:"
echo "home: $success_home"
echo "students: $success_students"
echo "docs: $success_docs"
echo
echo "File:"
echo "david: $success_students_david"
echo "juan: $success_students_juan"
echo "paul: $success_students_paul"
echo "README: $success_readme"
echo "sop.docs: $success_sop"
echo "sop.template: $success_template"
echo
echo "-------------------------"
echo "---- Infrastructure Team ----"
echo "-------------------------"
echo
echo "Directory:"
echo "home: $infrastructure_home"
echo "admin: $infrastructure_admin"
echo "scripts: $infrastructure_scripts"
echo "etc: $infrastructure_etc"
echo "logs: $infrastructure_logs"
echo "bin: $infrastructure_bin"
echo
echo "File:"
echo "README: $infrastructure_readme"
echo "vm_deploy.ymal: $vm_deploy"
echo "vm_destroy.ymal: $vm_destroy"
echo "hosts.tmp: $etc_hosts"
echo "fstab.tmp: $etc_fstab"

############ Yellow Tail Tech #############
#        Project # 2
#        Verify file manuplation 
###########################################  

#!/bin/bash

# Function to verify if a file exists
verify_file_exists() {
    if [ -e "$1" ]; then
        echo "[PASS] File $1 exists"
    else
        echo "[FAIL] File $1 does not exist"
    fi
}

# Function to verify if a file is missing
verify_file_missing() {
    if [ ! -e "$1" ]; then
        echo "[PASS] File $1 is missing"
    else
        echo "[FAIL] File $1 should be deleted but still exists"
    fi
}

# Function to verify if a file content is copied
verify_file_content_copied() {
    if cmp -s "$1" "$2"; then
        echo "[PASS] File content of $1 is copied to $2"
    else
        echo "[FAIL] File content of $1 is not copied to $2"
    fi
}

# Function to verify if a file is renamed
verify_file_renamed() {
    if [ ! -e "$1" ] && [ -e "$2" ]; then
        echo "[PASS] File $1 is renamed to $2"
    else
        echo "[FAIL] File $1 should be renamed to $2 but not found"
    fi
}

# Function to verify if a file is moved
verify_file_moved() {
    if [ ! -e "$1" ] && [ -e "$2" ]; then
        echo "[PASS] File $1 is moved to $2"
    else
        echo "[FAIL] File $1 should be moved to $2 but not found"
    fi
}

echo
echo "------------------------------------------------------------------"
echo "---- Verification begins ----"
echo "------------------------------------------------------------------"

# Verify renamed files
echo "Verify renamed files"
echo "--------------------"
verify_file_renamed "/yellow-tail-tech/enrollment/docs/sop.docs" "/yellow-tail-tech/enrollment/docs/sop.txt"
verify_file_renamed "/yellow-tail-tech/success/docs/sop.docs" "/yellow-tail-tech/success/docs/sop.txt"

echo
# Verify deleted files
echo "Verify deleted files"
echo "--------------------"
verify_file_missing "/yellow-tail-tech/enrollment/docs/sop.template"
verify_file_missing "/yellow-tail-tech/success/docs/sop.docs"
echo
# Verify copied files
echo "Verify copied files"
echo "--------------------"
verify_file_content_copied "/yellow-tail-tech/success/home/README" "/yellow-tail-tech/infrastructure/home/README"
verify_file_content_copied "/var/log/messages" "/yellow-tail-tech/infrastructure/logs/messages"
verify_file_content_copied "/etc/passwd" "/yellow-tail-tech/infrastructure/etc/passwd"
verify_file_content_copied "/bin/yum" "/yellow-tail-tech/infrastructure/bin/yum"
echo

# Verify moved files
echo "Verify moved files"
echo "--------------------"
verify_file_moved "/yellow-tail-tech/infrastructure/etc/hosts.tmp" "/yellow-tail-tech/infrastructure/home/admin/hosts.tmp"
verify_file_moved "/yellow-tail-tech/infrastructure/etc/fstab.tmp" "/yellow-tail-tech/infrastructure/home/admin/fstab.tmp"

echo
echo "------------------------------------------------------------------"
echo "---- Verify find output is copied in infrastrucre/config file ----"
echo "------------------------------------------------------------------"
echo "Find results:"
echo "Files with .conf extension:"
tail "/yellow-tail-tech/infrastructure/etc/configs"
echo
echo "------------------------------------------------------------------"
echo "---- Verify find output is copied to infrastrucre/600-files ------"
echo "------------------------------------------------------------------"
echo
echo "Files with permissions 600"
tail "/yellow-tail-tech/infrastructure/etc/600-files"
echo
echo "------------------------------------------------------------------"
echo "---- Verification completed ----"
echo "------------------------------------------------------------------"
echo

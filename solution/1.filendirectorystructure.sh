#/bin/bash

# Create Directories for Enrollment Team

mkdir -p /yellow-tail-tech/enrollment/home/
mkdir -p /yellow-tail-tech/enrollment/scripts/
mkdir -p /yellow-tail-tech/enrollment/docs/

# Create files for Enrollment Team

touch /yellow-tail-tech/enrollment/home/README
touch /yellow-tail-tech/enrollment/scripts/start_enrollment.py
touch /yellow-tail-tech/enrollment/scripts/stop_enrollment.py
touch /yellow-tail-tech/enrollment/docs/enrollment_data.csv
touch /yellow-tail-tech/enrollment/docs/sop.docs
touch /yellow-tail-tech/enrollment/docs/sop.template


# Create Directories for Student Success Team

mkdir -p /yellow-tail-tech/success/home/
mkdir -p /yellow-tail-tech/success/students/
mkdir -p /yellow-tail-tech/success/docs/


# Create files for Student Success Team

touch /yellow-tail-tech/success/home/README
touch /yellow-tail-tech/success/students/juan
touch /yellow-tail-tech/success/students/david
touch /yellow-tail-tech/success/students/paul
touch /yellow-tail-tech/success/docs/sop.docs
touch /yellow-tail-tech/success/docs/sop.template

# Create Directories for Infrastructure Team

mkdir -p /yellow-tail-tech/infrastructure/home/
mkdir -p /yellow-tail-tech/infrastructure/home/admin/
mkdir -p /yellow-tail-tech/infrastructure/scripts/
mkdir -p /yellow-tail-tech/infrastructure/etc/
mkdir -p /yellow-tail-tech/infrastructure/logs/
mkdir -p /yellow-tail-tech/infrastructure/bin/

# Create files for Infrastructure Team

touch /yellow-tail-tech/infrastructure/home/README
touch /yellow-tail-tech/infrastructure/scripts/vm_deploy.ymal
touch /yellow-tail-tech/infrastructure/scripts/vm_destroy.ymal
touch /yellow-tail-tech/infrastructure/etc/hosts.tmp
touch /yellow-tail-tech/infrastructure/etc/fstab.tmp

# Data entry

echo "Project name and introduction (required)" >> /yellow-tail-tech/enrollment/home/README
echo "Table of contents (optional)" >> /yellow-tail-tech/enrollment/home/README
echo "Requirements (required)" >> /yellow-tail-tech/enrollment/home/README
echo "Recommended modules (optional)" >> /yellow-tail-tech/enrollment/home/README
echo "Installation (required, unless a separate INSTALL.md is provided)" >> /yellow-tail-tech/enrollment/home/README
echo "Configuration (required)" >> /yellow-tail-tech/enrollment/home/README
echo "Troubleshooting & FAQ (optional)" >> /yellow-tail-tech/enrollment/home/README
echo "Maintainers (optional)" >> /yellow-tail-tech/enrollment/home/README


echo "Project name and introduction (required)" >> /yellow-tail-tech/success/home/README
echo "Table of contents (optional)" >> /yellow-tail-tech/success/home/README
echo "Requirements (required)" >> /yellow-tail-tech/success/home/README
echo "Recommended modules (optional)" >> /yellow-tail-tech/success/home/README
echo "Installation (required, unless a separate INSTALL.md is provided)" >> /yellow-tail-tech/success/home/README
echo "Configuration (required)" >> /yellow-tail-tech/success/home/README
echo "Troubleshooting & FAQ (optional)" >> /yellow-tail-tech/success/home/README
echo "Maintainers (optional)" >> /yellow-tail-tech/success/home/README

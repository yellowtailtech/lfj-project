#!/bin/bash

# Delete Files
rm -f /yellow-tail-tech/enrollment/docs/sop.template
rm -f /yellow-tail-tech/enrollment/docs/sop.template

# Copy Files

cp  /yellow-tail-tech/success/home/README /yellow-tail-tech/infrastructure/home/README
cp  /var/log/messages /yellow-tail-tech/infrastructure/logs/
cp  /etc/passwd /yellow-tail-tech/infrastructure/etc/
cp  /bin/yum   /yellow-tail-tech/infrastructure/bin/

# Rename Files

mv /yellow-tail-tech/enrollment/docs/sop.docs /yellow-tail-tech/enrollment/docs/sop.txt
mv /yellow-tail-tech/success/docs/sop.docs    /yellow-tail-tech/success/docs/sop.txt

# Move files

mv /yellow-tail-tech/infrastructure/etc/hosts.tmp  /yellow-tail-tech/infrastructure/home/admin/
mv /yellow-tail-tech/infrastructure/etc/fstab.tmp  /yellow-tail-tech/infrastructure/home/admin/

#Find Files

find /etc/  -name *.conf >> /yellow-tail-tech/infrastructure/etc/configs
find /etc/  -perm 600   >> /yellow-tail-tech/infrastructure/etc/600-files
